<?php
    $aColumns     = array(
        'created',
        'title',
        'description',
        'tags',
        'creator',
        '0 as action'
    );
    $sIndexColumn = 'id';
    $sTable       = 'article';

    $join         = array();
    $additionalSelect = array(
        'content',
        'id'
    );

    $where = array();
    $filter = array();

    // filter something
    array_push($filter, 'AND (IFNULL(deleted,0) = 0)');

    // where with filter
    if (count($filter) > 0) {
        array_push($where, 'AND ('.Helper::filterDataTable($filter).')');
    }

    $result           = Helper::createDataTable($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);
    $output           = $result['output'];
    $rResult          = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if (strpos($aColumns[$i],'as') !== false && !isset($aRow[ $aColumns[$i] ])){
                $_data = $aRow[ Helper::string_after($aColumns[$i], 'as ')];
            } else {
                $_data = $aRow[ $aColumns[$i] ];
            }

            if ($aColumns[$i] == $sIndexColumn || $aColumns[$i] == 'title') {
                $_data = '<a class="center-block text-center text-nowrap" href="../article-' . $aRow['id'] . '-' . Helper::urlencode($aRow['title']) . '.html" target="_blank">' . $aRow['title'] . '</span>';
            }
            if ($aColumns[$i] == 'created') {
                $_data = '<span class="center-block text-center text-nowrap">' . date('Y-m-d H:i', strtotime($_data)) . '</span>';
            }
            if ($aColumns[$i] == 'creator') {
                $_data = explode('@', $_data)[0];
            }
            if ($aColumns[$i] == 'description') {
                $preview = array();
                if ($attachment = DB::table('attachment')->where('related_table', 'article')->where('related_id', $aRow['id'])->first()) {
                    $preview[] = '<a href="javascript:void(0)" value="' . url('files/attachment_' . $attachment->id . $attachment->file_ext) . '" class="imgpreview"><i class="fa fa-picture-o pull-left noprint" data-toggle="tooltip" data-html="true" data-placement="bottom" title="&lt;img style=&quot;width:100px;height:auto;&quot; src=&quot;' . url('files/attachment_' . $attachment->id . '_thumb' . $attachment->file_ext) . '&quot; /&gt;"></i></a>';
                }
                preg_match_all('/<img[^>]+>/i', $aRow['content'], $images);
                foreach (!empty($images) && !empty($images[0])? $images[0]: array() as $img) {
                    $imageurl = Helper::string_between('src="', '"', $img);
                    $preview[] = '<a href="javascript:void(0)" value="' . $imageurl . '" class="imgpreview"><i class="fa fa-picture-o pull-left noprint" data-toggle="tooltip" data-html="true" title="&lt;img style=&quot;width:100px;height:auto;&quot; src=&quot;' . $imageurl . '&quot; /&gt;"></i></a>';
                }
                $_data = implode('', $preview) . '<div class="clearfix"></div>' . $_data;
            }
            if (stripos($aColumns[$i], 'action') !== false) {
                $_data = '<div class="center-block text-center">';
                $_data.= '<a href="./article/' . $aRow['id'] . '?url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i></button></a>';
                $_data.= '&nbsp;';
                $_data.= '<a href="?del=' . $aRow['id'] . '&url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-remove"></i></button></a>';
                $_data.= '</div>';
            }

            $row[] = $_data;
        }

        $output['aaData'][] = $row;
    }

    header('Content-Type: application/json');
    echo json_encode($output);
