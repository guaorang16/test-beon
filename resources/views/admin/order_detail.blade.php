@extends('admin')

@section('title', (!empty($id)? 'Detail' : 'Details') . ' Order')

@section('content')

<h3 class="well">@yield('title')</h3>

<div id="container">
    <form>
        @csrf
        <table style="border:0; width:100%;">
            <tr>
                <td>Order Number :</td>
                <td>
                    <input type="text" name="description" value="{{ $ordernum }}" class="btn btn-default" />
                </td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>
                    <input type="text" name="description" value="{{ $name }}" class="btn btn-default" />
                </td>
            </tr>
            <tr>
                <td>Payment Method:</td>
                <td>
                    <input type="text" name="description" value="{{ $paymentmethod }}" class="btn btn-default" />
                </td>
            </tr>
            <tr>
                <td>Payment Status:</td>
                <td>
                    <input type="text" name="description" value="{{ $status }}" class="btn btn-default" />
                </td>
            </tr>
            <tr>
                <td>Amount :</td>
                <td>
                    <input type="text" name="description" value="{{ $currencyprefix }}{{ $amount }}" class="btn btn-default" />
                </td>
            </tr>
        </table>
    </form>
    <div id="container">
    <div class="col-md-12">
        <table class="table table-striped table-bordered table-condensed" id="table_data">
            <thead>
                <tr>
                    <th class="text-left">Type</th>
                    <th class="text-left">Product</th>
                    <th class="text-left">Domain</th>
                    <th class="text-left">Amount</th>
                    <th class="text-left">Status</th>
                </tr>
            </thead>
            <tbody>
            @foreach($lineitem as $data)
                <tr>
                    <td>{{ $data['producttype'] }}</td>
                    <td>{{ $data['product'] }}</td>
                    <td>{{ $data['domain'] }}</td>
                    <td>{{ $data['amount'] }}</td>
                    <td>{{ $data['status'] }}</td>
                </tr>
            @endforeach
        </tbody>
        </table>
    </div>
</div>
    <a class="btn btn-primary pull-right" href="{{ url()->previous() }}" class="btn btn-primary">Go Back</a>
</div>

@endsection

@section('javascript')
@parent

<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<link href="{{ url('/'); }}/public/assets/admin/toastr.min.css" rel="stylesheet" />
<script src="{{ url('/'); }}/public/assets/admin/toastr.min.js"></script>

<script src="{{ url('/'); }}/public/assets/admin/fileupload.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        initDataTable('table_data', '{{ url('/list_order'); }}', 'undefined', 'undefined', [], [0, 'ASC']);
    });
</script>

@endsection

@section('stylesheet')
@parent

<style>
    td {
        padding-right: 15px;
        padding-bottom: 10px;
    }
    input[type=text], select, textarea {
        text-align: left !important;
        width: 100%;
    }
    .required:after {
        content: '*';
        color: red;
        margin-left: 5px;
    }
</style>

@endsection