@extends('admin')

@section('title', 'List Message')

@section('content')

<h3 class="well">@yield('title')</h3>

<div id="container">
	<div class="hidden">
		<select id="status" name="status"  class="datatable-filters form-control input-sm">
            <option value="0">Unread</option>
            <option value="1">Read</option>
            <option value="">All Status</option>
        </select>
		<input id="from" name="pstart" type="text" placeholder="From" class="datatable-filters form-control input-sm" />
		<input id="until" name="puntil" type="text" placeholder="Until" class="datatable-filters form-control input-sm" />
  	</div>
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-left">Date</th>
					<th class="text-left">Name</th>
					<th class="text-left">Email</th>
					<th class="text-left">Phone</th>
					<th class="text-left">Message</th>
					<th class="text-center" style="width:50px">&nbsp;</th>
				</tr>
			</thead>
        </table>
	</div>
</div>

@endsection

@section('javascript')
@parent

<script type="text/javascript">
	function mark(id, state) {
		$.post('{{ url('/admin'); }}/message?read=' + state, {id: id}, function(data) {
			if (data.result) {
				$('.table').DataTable().ajax.reload();
			}
		});
	}

	function set_datepicker(obj) {
		var setdate = $(obj).datepicker('getDate');
		if (typeof setdate.setDate != 'undefined') setdate.setDate(setdate.getDate());
		$('#until').datepicker('option','minDate',setdate); 
	}

	jQuery(document).ready(function($) {
		$('#from, #until').datepicker({
			changeMonth: false,
			changeYear: true, 
            showOtherMonths: true,    // new code
            selectOtherMonths: true,  // new code
            firstDay: 1,               // new code
			minDate: new Date(2021, 1 - 1, 1), 
			dateFormat: "yy-mm-dd",
			'onSelect': function(dateStr){
				if ($(this).attr('id')!='until') set_datepicker(this);
				$(this).trigger('change');
			}
		}).attr('autocomplete', 'off');
		
		$('#from, #until, #status').on('change', function(){
			$('.table').DataTable().ajax.reload();
		});

		var searchparam = {};
	    $('.datatable-filters').each(function(){
	        searchparam[$(this).attr('name')] = '[name="'+$(this).attr('name')+'"]';
	    });
		initDataTable('.table', '{{ url('/admin'); }}/message', 'undefined', 'undefined', searchparam, [0, 'ASC']);

		$('#from').css('width', '100px').appendTo('.dataTables_filter');
		$('#until').css('width', '100px').appendTo('.dataTables_filter');
		$('#status').appendTo('.dataTables_filter');
	});
</script>

@endsection

@section('stylesheets')
@parent

<style type="text/css">
	.table [tabindex="-0"] span {
		cursor: pointer;
	}
</style>

@endsection
