<?php
    $aColumns     = array(
        '`date` as created',
        'name',
        'email',
        'phone',
        'text',
        '0 as action'
    );
    $sIndexColumn = 'id';
    $sTable       = 'message';

    $join         = array();
    $additionalSelect = array(
        '`read` as mark',
        'id'
    );

    $where = array();
    $filter = array();

    // filter date
    if (!empty($_POST['pstart'])) {
        array_push($filter, 'AND (DATE(`date`) >= "' . date('Y-m-d', strtotime($_POST['pstart'])) . '")');
    }
    if (!empty($_POST['puntil'])) {
        array_push($filter, 'AND (DATE(`date`) <= "' . date('Y-m-d', strtotime($_POST['puntil'])) . '")');
    }

    // filter category
    if (isset($_POST['status']) && trim($_POST['status']) != '') {
        array_push($where, 'AND (`read` = ' . $_POST['status'] . ')');
    }

    // where with filter
    if (count($filter) > 0) {
        array_push($where, 'AND ('.Helper::filterDataTable($filter).')');
    }

    $result           = Helper::createDataTable($aColumns, $sIndexColumn, $sTable, $join, $where, $additionalSelect);
    $output           = $result['output'];
    $rResult          = $result['rResult'];

    foreach ($rResult as $aRow) {
        $row = array();
        for ($i = 0; $i < count($aColumns); $i++) {
            if (strpos($aColumns[$i],'as') !== false && !isset($aRow[ $aColumns[$i] ])){
                $_data = $aRow[ Helper::string_after($aColumns[$i], 'as ')];
            } else {
                $_data = $aRow[ $aColumns[$i] ];
            }

            if ($aColumns[$i] == $sIndexColumn || $aColumns[$i] == 'title') {
                $_data = '<a class="center-block text-center text-nowrap" href="../article/' . str_replace('=', '', base64_encode($aRow['id'])) . '/' . urlencode(strtolower($aRow['title'])) . '" target="_blank">' . $aRow['title'] . '</span>';
            }
            if ($aColumns[$i] == 'text') {
                $_data = '<span class="text-nowrap">' . nl2br($_data) . '</span>';
            }
            if ($aColumns[$i] == 'created') {
                $_data = '<span class="center-block text-center text-nowrap">' . date('Y-m-d H:i', strtotime($_data)) . '</span>';
            }
            if (stripos($aColumns[$i], 'action') !== false) {
                $_data = '<div class="center-block text-center">';
                //$_data.= '<a href="./article/' . $aRow['id'] . '?url=' . base64_encode($_SERVER['HTTP_REFERER']) . '"><button><i class="fa fa-edit"></i></button></a>';
                //$_data.= '&nbsp;';
                if (!empty($aRow['mark'])) {
                    $_data.= '<button onclick="mark(' . $aRow['id'] . ',0)" title="Mark as unread"><i class="fa fa-folder"></i></button>';
                } else {
                    $_data.= '<button onclick="mark(' . $aRow['id'] . ',1)" title="Mark as read"><i class="fa fa-folder-open"></i></button>';
                }
                $_data.= '</div>';
            }

            $row[] = $_data;
        }

        $output['aaData'][] = $row;
    }

    header('Content-Type: application/json');
    echo json_encode($output);
