@extends('admin')

@section('title', 'List Article')

@section('content')

<h3 class="well">@yield('title')</h3>
$data: {{ $data }}
<a href="{{ url('/admin'); }}/article/new?url={{ base64_encode(Helper::current_url()); }}" class="btn btn-success pull-right" style="height:34px; margin:-70px 15px;">
	<i class="fa fa-plus visible-xs"></i> <span class="hidden-xs">Add Data</span>
</a>

<div id="container">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-left">Date</th>
					<th class="text-left">Title</th>
					<th class="text-left">Description</th>
					<th class="text-left">Tags</th>
					<th class="text-left">Creator</th>
					<th class="text-center" style="width:50px">&nbsp;</th>
				</tr>
			</thead>
        </table>
	</div>
</div>

@endsection

@section('javascript')
@parent

<script type="text/javascript">
	jQuery(document).ready(function($) {
		initDataTable('.table', '{{ url('/admin'); }}/article', 'undefined', 'undefined', [], [0, 'ASC']);
	});
</script>

@endsection

@section('stylesheets')
@parent

<style type="text/css">
	.table [tabindex="-0"] span {
		cursor: pointer;
	}
</style>

@endsection
