<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Admin Website - Login</title>

    <link href="{{ url('/') }}/public/assets/admin/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ url('/') }}/public/assets/admin/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ url('/') }}/public/assets/favicon.ico" rel="shortcut icon" />
    <style type="text/css">
        .form-signin
        {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-control
        {
            text-align: center;
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .form-signin .form-control:focus
        {
            z-index: 2;
        }
        .form-signin input[type="password"]
        {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }
        .form-signin input[type="submit"]
        {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
        .form-signin input[type="text"], .form-signin select {
            border-radius: 0;
            margin-bottom: -1px;
        }
        .account-wall
        {
            margin-top: 80px;
            padding: 40px 0px 20px 0px;
            background-color: #fff;
            box-shadow: 0 5px 20px 0 rgba(0, 0, 0, 0.15);
        }
        .account-img
        {
            width: 100px;
            height: auto;
            margin: 0 auto 10px;
            display: block;
        }
        h4 {
            text-align: center;
            margin: 20px 0 0;
        }
        a {
            color: #404040;
        }
        a:hover {
            color: auto;
            text-decoration: none;
        }
    </style>
</head>
<body>
    <div class="container">
        @if ($error)
        <div class="alert alert-danger col-md-12" style="margin-top:10px;">
            <b>Warning:</b> {{ $error; }}
        </div>
        @endif
        @if (session('status'))
        <div class="alert alert-success">
            <b>Notifications :</b> {{ session('status') }}
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <a href="./">
                        <img class="account-img" src="{{ url('/') }}/public/assets/img/sleep-logo.png" />
                    </a>
                    <h4>
                        LOGIN
                    </h4>
                    <form class="form-signin" method="post" enctype="multipart/form-data">
                        @csrf
                        <input type="text" class="form-control" name="email" placeholder="Username" required />
                        <input type="password" class="form-control" name="password" placeholder="Password" required />
                        <input type="submit" class="btn btn-lg btn-default btn-block" value="Login" />

                        <a href="{{ url('/register') }}">
                            <input type="button" class="btn btn-lg btn-default btn-block" value="Register" />
                        </a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.getElementsByTagName('input')[0].focus();
    </script>
</body>
</html>
