Hello, {{ session()->get('userid')? session()->get('username'): 'Guest' }}

<br/>
<br/>

@if(session()->get('userid'))
	<a href="logout">Logout</a>
@else
	<a href="login">Login here!</a> |
	<a href="register">Register</a>
@endif