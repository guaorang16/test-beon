<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="expires" content="Wed May 18 2022 16:00:07 GMT+0700">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<meta name="format-detection" content="telephone=no">
	<meta name="it-rating" content="it-rat-cd303c3f80473535b3c667d0d67a7a11">
	<meta name="cmsmagazine" content="3f86e43372e678604d35804a67860df7">

	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="description" content="" />
    <meta name="author" content="" />

	<title>NJY Beauty Studio - @yield('title', 'Home')</title>

@section('stylesheet')
	<link rel="preload stylesheet" href="css/style.css" as="style">
	<style>
		body {
		    background-color: #0b2525;
		}

	    /* header menu */
		.header-content .header-nav > li > a,
		.header-content .header-options > li i:before {
			color: #c5b34b !important;
			-webkit-filter: drop-shadow(1px 1px 0 #170d01) drop-shadow(-1px -1px 0 #170d01);
		}
		.header-content .header-nav > li > a:hover,
		.header-options > li i:hover {
			color: #5ddbff;
		}
		.header-nav li ul {
			background-color: #c5b34b;
		}
		.header-nav li ul li a {
			color: #0b2525 !important;
		}
		.header-nav li ul li a:hover {
			background-color: #5ddbff;
		}
		.header-nav li a:after {
			color: #170d01;
		}
		/* cart number */
		.header-options li span {
			color: #170d01;
    		background-color: #5ddbff;
		}
		/* header & logo */
		.header-content.fixed {
		    background-color: #0b2525;
		}
		.header-content.fixed .header-logo img.logo-light {
			display: block;
		}
		.header-content.fixed .header-logo img.logo-dark {
			display: none;
		}
		.header-content .header-logo img.logo-light {
			display: none;
		}
		.header-content .header-logo img.logo-dark {
			display: block;
		}
		.header-logo img {
			height: 60px;
		}

		/* banner */
		.main-wrapper {
			background-image: url("./img/content-texture-bg.png");
			background-size: 100% auto;
			background-repeat: repeat-y;
			z-index: 0;
		}
		.main-block {
			background-image: url("./img/bg-header.png");
			background-position: bottom;
		}
		.detail-block {
			background-image: url("./img/bg-header-alt.png");
			background-position: top;
		}
		.main-text {
			font-family: "Mrs Saint Delafield";
		    -webkit-filter: drop-shadow(3px 3px 0 #170d01) drop-shadow(-1px -1px 0 #170d01);
		    background: linear-gradient(to bottom, #cdba5d 40%, #8d7636 50%, #e0c570 60%);
		    -webkit-text-fill-color: transparent;
		    -webkit-background-clip: text;
		}

		@media screen and (max-width: 991px) {
			.main-block .invisible {
				visibility: visible !important;
			}
			.main-block .hidden {
				display: block !important;
			}
			.main-block .hidden img {
			    height: 150px;
			    -webkit-filter: drop-shadow(3px 3px 0 #170d01) drop-shadow(-1px -1px 0 #170d01);
			}
		}

		/* product */
		.nav-tab-list li.active a {
		    background: #5ddbff;
		    border-color: #5ddbff;
		}
		.products-item .icon-search:before {
		    color: #c5b34b;
		}
		.products-item__sale {
		    background: #c5b34b;
		}
		.products-item__new {
		    background: #0b2525;
		}
		.products-item__hover-options i.active,
		.products-item__hover-options i.icon-cart {
		    background: #0b2525;
		}
		.products-item__name,
		.products-item__cost {
			color: #fff;
		}
		.products-item__cost span {
			color: #666;
		}
		.product-buttons [class^="icon-"]:before {
		    color: inherit;
		}

		.product-price,
		.products-item__cost {
			display: none;
		}

		/* advantage */
		.advantages-item__icon {
			margin-bottom: 10px;
		}
		.advantages-item__icon img {
			max-height: 120px;
		}

		/* info */
		.info-blocks__item,
		.info-blocks__item-reverse {
			background-color: #0008087a;
			z-index: 1;
		}.info-blocks__item-reverse {
			background-size: contain;
		}
		.info-blocks__item-link {
			color: #5ddbff;
		}

		/* blog */
		.blog-item__title {
		    color: #fff;
		}
		.blog-item__title:hover {
		    color: #5ddbff;
		}
		.icon-arrow-md:before {
		    color: #5ddbff;
		}

		/* footer */
		.footer {
		    background-color: #0b2525;
		}
		.footer-top__logo img {
			max-height: 60px;
			max-width: auto;
		}


		/* other */
		.hidden {
			display: none !important;
		}
		.invisible {
			visibility: hidden !important;
		}
		.nopad {
			padding: 0 !important;
		}

		h1 {
			color: #fff !important;
			-webkit-filter: drop-shadow(1px 1px 0 #170d01) drop-shadow(-1px -1px 0 #170d01);
		}
		h1, h2, h3, h4, p {
			color: #fff !important;
		}
		a {
			color: #5ddbff;
		}
		.saint-text {
			color: #c5b34b;
			margin-bottom: 30px;
			word-spacing: 15px;
		}
		.saint-text:not(.hidden):not(.invisible):after {
			background: url(img/title-text-decor.png) center no-repeat;
		    background-size: 100% auto;

		    content: "";
		    position: absolute;
		    -webkit-transform: translate(-75%, -35%);
		    -ms-transform: translate(-75%, -35%);
		    transform: translate(-75%, -35%);
		    width: 300px;
		    height: 100px;
		    z-index: -1;
		}

		.btn-menu span,
		.btn {
			background-color: #c5b34b;
		}
		.btn-menu span:hover,
		.btn:hover {
		    background: #5ddbff;
		}
		.btn {
		    color: #170d01;
		    border: 1px solid #170d01;
		    border-radius: 10px;
		    -webkit-filter: drop-shadow(0 0 0 #170d01) drop-shadow(5px 5px 5px #170d01);
		}

		[class^="icon-"]:before {
		    color: #5ddbff;
		}
		.icon-load {
		    filter: hue-rotate(75deg) brightness(2);
		}
	</style>
@show

	<link rel="icon" type="image/x-icon" href="img/favicon.ico" />
</head>

<body class="loaded">

	<!-- BEGIN BODY -->

	<div class="main-wrapper">

		<!-- BEGIN CONTENT -->

		<main class="content">

@yield('content')

			<!-- BEGIN INSTA PHOTOS -->
			<div class="insta-photos invisible" style="height:148px;">
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
				<a href="#" class="insta-photo">
					<img data-src="https://via.placeholder.com/320/004545" src="data:image/gif;base64,R0lGODlhAQABAAAAACw=" class="js-img"
						alt="">
					<div class="insta-photo__hover">
						<i class="icon-insta"></i>
					</div>
				</a>
			</div>
			<!-- INSTA PHOTOS EOF   -->

		</main>

		<!-- CONTENT EOF   -->

		<!-- BEGIN HEADER -->

		<header class="header">
			<div class="header-top hidden">
				<span>30% OFF ON ALL PRODUCTS ENTER CODE: NJY2020</span>
				<i class="header-top-close js-header-top-close icon-close"></i>
			</div>
			<div class="header-content">
				<div class="header-logo">
					<img class="logo-dark" src="img/header-logo-horiz.png" alt="">
					<img class="logo-light" src="img/header-logo-horiz.png" alt="">
				</div>
				<div class="header-box">
					<ul class="header-nav">
						<li><a href="index.html">Beranda</a></li>
						<li><a href="categories.html">Kategori</a>
							<ul>
								<li><a href="category-spa.html">SPA</a></li>
								<li><a href="category-nails.html">Nails</a></li>
								<li><a href="category-eyelash.html">Eyelash</a></li>
								<li><a href="category-hair-care.html">Hair Care</a></li>
							</ul>
						</li>
						<li><a href="articles.html">Artikel</a></li>
						<li><a href="contacts.html">Kontak</a></li>
					</ul>
					<ul class="header-options">
						<!--
						<li><a href="#"><i class="icon-search"></i></a></li>
						<li><a href="#"><i class="icon-user"></i></a></li>
						<li><a href="wishlist.html"><i class="icon-heart"></i></a></li>
						<li><a href="cart.html"><i class="icon-cart"></i><span>0</span></a></li>
						-->
					</ul>
				</div>

				<div class="btn-menu js-btn-menu"><span>&nbsp;</span><span>&nbsp;</span><span>&nbsp;</span></div>
			</div>
		</header>

		<!-- HEADER EOF   -->

		<!-- BEGIN FOOTER -->

		<footer class="footer">
			<div class="wrapper">
				<div class="footer-nav">
					<div class="footer-nav__col">
						<span class="footer-nav__col-title">Tentang</span>
						<ul>
							<li><a href="articles.html">Artikel</a></li>
							<li><a href="categories.html">Kategori</a></li>
							<li><a href="about.html">Tentang Kami</a></li>
							<li><a href="contacts.html">Hubungi Kami</a></li>
						</ul>
					</div>
					<div class="footer-nav__col">
						<span class="footer-nav__col-title">Kategori</span>
						<ul>
							<li><a href="category-spa.html">SPA</a></li>
							<li><a href="category-nails.html">Nails</a></li>
							<li><a href="category-eyelash.html">Eyelash</a></li>
							<li><a href="category-hair-care.html">Hair Care</a></li>
						</ul>
					</div>
					<div class="footer-nav__col invisible">
						<!--
						<span class="footer-nav__col-title">Useful links</span>
						<ul>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms Of Use</a></li>
						</ul>
						-->
					</div>
					<div class="footer-nav__col">
						<span class="footer-nav__col-title">Kontak</span>
						<ul>
							<li>
								<i class="icon-map-pin"></i>
								Kota Malang, Jawa Timur
							</li>
							<li>
								<i class="icon-insta"></i>
								<a href="https://www.instagram.com/njybeautystudio">@njybeautystudio</a>
							</li>
							<li>
								<i class="icon-mail"></i>
								<a href="mailto:info@njybeauty.com">info@njybeauty.com</a>
							</li>
							<li>
								<i class="icon-smartphone"></i>
								<a href="tel:+6281233730288">0812 33 7302 88</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="footer-copy">
					<span>NJY Beauty Studio &copy; 2022</span>
				</div>
			</div>
		</footer>

		<!-- FOOTER EOF   -->

	</div>

	<div class="icon-load"></div>

	<!-- BODY EOF   -->

@section('javascript')
	<script src="js/jquery-3.5.1.min.js"></script>
	<script src="js/lazyload.min.js"></script>
	<script src="js/slick.min.js"></script>
	<script src="js/custom.js"></script>
	<script>
		$(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			var page = document.location.href.indexOf('.html') > 0? document.location.href.split('/').pop(): 'index.html';
			$('.header-nav a').each(function() {
				if (page == $(this).attr('href')) {
					if (!$(this).closest('ul').hasClass('header-nav')) {
						$(this).closest('ul').prev().addClass('active');
					} else {
						$(this).addClass('active');
					}
				}
			});
		});
	</script>
@show

</body>
</html>
