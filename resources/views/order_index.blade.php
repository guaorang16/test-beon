@extends('admin')

@section('title', 'List Order')

@section('content')

<h3 class="well">@yield('title')</h3>
<a href="{{ url('/add_order'); }}" class="btn btn-success pull-right" style="height:34px; margin:-70px 15px;">
	<i class="fa fa-plus visible-xs"></i> <span class="hidden-xs">New Order</span>
</a>

<div id="container">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed" id="table_data">
			<thead>
				<tr>
					<th class="text-left">No Order</th>
					<th class="text-left">Payment Method</th>
					<th class="text-left">Status</th>
					<th class="text-left">Date</th>
					<th class="text-center" style="width:50px">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
            @foreach($order as $data)
                <tr>
                    <td>{{ $data['ordernum'] }}</td>
                    <td>{{ $data['paymentmethod'] }}</td>
                    <td>{{ $data['status'] }}</td>
                    <td>{{ $data['date'] }}</td>
                    <td><a href="/detail_order/{{ $data['id'] }}"><button><i class="fa fa-clipboard"></i></button></a></td>
                </tr>
            @endforeach
        </tbody>
        </table>
	</div>
</div>

@endsection

@section('javascript')
@parent

<script type="text/javascript">
	jQuery(document).ready(function($) {
		initDataTable('table_data', '{{ url('/list_order'); }}', 'undefined', 'undefined', [], [0, 'ASC']);
	});
</script>

@endsection

@section('stylesheets')
@parent

<style type="text/css">
	.table [tabindex="-0"] span {
		cursor: pointer;
	}
</style>

@endsection