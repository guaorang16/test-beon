<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Login;
use App\Http\Controllers\Order;
use App\Http\Controllers\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('admin/welcome');
});

Route::any('/order', [Order::class, 'submit']);

Route::any('/list_order', [Order::class, 'list']);
Route::any('/detail_order/{id}', [Order::class, 'detail']);
Route::any('/detail_orders/{id}', [Order::class, 'details']);
Route::any('/add_order', [Order::class, 'save']);

Route::post('/dashboard', [User::class, 'dashboard']);

//Route::resource('/user', User::class);
//Route::any('/{page}.html', [App\Http\Controllers\Order::class , 'page']);

Route::any('/whmcs/{action?}', function ($action = '') {
	return response()->json(App\Whmcs::getData($action, request()->all()), 200);
});

Route::any('/login', [Login::class , 'authenticate']);
Route::any('/logout', [Login::class , 'relogin']);
Route::any('/register', [Login::class , 'register']);