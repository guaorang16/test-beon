var str = {
    str: function(val) {
        return val == null || typeof val == 'undefined' ? '' : val;
    },
    trim: function(val) {
        return $.trim(str.str(val));
    },
    upper: function(val) {
        return str.trim(val).toUpperCase();
    },
    lower: function(val) {
        return str.trim(val).toLowerCase();
    },
    empty: function(val, number) {
        if (typeof(number) == 'undefined') number = false;
        if (number && str.number(val) == 0) val = '';
        return str.trim(val) == '';
    },
    escape: function(val) {
        return str.str(val).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
    },
    between: function(front, back, val) {
        return str.trim(val).substring(val.lastIndexOf(front) + 1, val.lastIndexOf(back))
    },
    truncate: function(string, length, separator, frontlength) {
        if (string.length <= length) return string;
        separator = separator || '...';

        var sepLen = separator.length,
            charsToShow = length - sepLen,
            frontChars = frontlength || Math.floor(charsToShow/2),
            backChars = charsToShow - frontChars;

        return string.substr(0, frontChars) + separator + string.substr(string.length - backChars);
    },
    htmlspecialchars: {
        encode: function(val) {
            var map = {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#039;'
            };
            return val.replace(/[&<>"']/g, function(m) { return map[m]; });
        },
        decode: function(val) {
            return str.trim(val).htmlspecialchars_decode();
        }
    },
    replace: function(find, change, val) {
        if (find instanceof Array) {
            $.each(find, function(i) {
                val = str.replace(this, change instanceof Array ? change[i] : change, val);
            });
            return val;
        }
        var reg = new RegExp(str.escape(find), 'ig');
        return str.trim(val).replace(reg, change);
    },
    number: function(val, decimal) {
        val = str.trim(val).match(/(\+|-)?((\d+(\.\d+)?)|(\.\d+))/);
        val = parseFloat(val && val[0] || 0);
        val = isNaN(val) ? 0 : val;
        return typeof decimal == 'undefined' ? val : parseFloat(val.toFixed(decimal));
    },
    decimal: function(val, separator, decimal, nozerodecimal) {
        if (str.empty(separator)) {
            separator = ',';
        }
        if (str.empty(decimal)) {
            decimal = 2;
        }
        val = str.number(val).toFixed(decimal).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        if (nozerodecimal === true) {
            val = val.replace('.' + '0'.repeat(decimal), '');
        }
        return val;
    },
    percent: function(val, sign, empty, decimal) {
        if (str.empty(sign)) {
            sign = '%';
        }
        if (typeof(empty) == 'undefined') {
            empty = '0';
        }
        if (str.empty(decimal)) {
            decimal = 2;
        }
        if (str.empty(val)) {
            val = empty;
            if (empty === '') {
                sign = '';
            }
        }
        if (val === '') {
            sign = '';
        } else {
            val = str.decimal(val, null, decimal, empty === '0');
        }
        return val + sign;
    },
    money: function(val, sign, empty, decimal) {
        if (str.empty(sign)) {
            sign = 'Rp ';
        }
        if (typeof(empty) == 'undefined') {
            empty = '0';
        }
        if (str.empty(decimal)) {
            decimal = 2;
        }
        if (str.empty(val)) {
            val = empty;
        }
        if (val === '') {
            sign = '';
        } else {
            val = str.decimal(val, null, decimal, false);
        }
        return sign + val;
    },
    phone: function(val) {
        var value = str.trim(val).replace(/ /g, ''),
            valid = value.replace(/ /g, '').match(/^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8]|13)\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$/);

        if (valid) {
            valid[2] = parseInt(valid[2]);
            value = (valid[1] ? (valid[1] + ' ') : '') + (valid[2] == 13 ? '' : '0') + valid[2];
            if (valid[2] == 4 || valid[2] == 13) {
                value += valid[3].substr(0, 2) + ' ' + valid[3].substr(2, 3) + ' ' + valid[3].substr(5, valid[3].length - 5);
            } else {
                value += ' ' + valid[3].substr(0, 4) + ' ' + valid[3].substr(4, valid[3].length - 4);
            }
        }

        return value;
    },
    timestamp: function() {
        var year = (new Date()).getFullYear();
        var month = (new Date()).getMonth();
        var day = (new Date()).getDate();
        var hour = (new Date()).getHours();
        var min = (new Date()).getMinutes();
        var sec = (new Date()).getSeconds();
        var ms = (new Date()).getMilliseconds();
        var now = Date.UTC(year, month, day, hour, min, sec, ms);
        return now;
    },
    datetime: function(date) {
        date = date || new Date();
        var Y = date.getFullYear().toString();
        var M = (date.getMonth() + 1).toString();
        M = M[1] ? M : ('0' + M[0]);
        var D = date.getDate().toString();
        D = D[1] ? D : ('0' + D[0]);
        var H = date.getHours().toString();
        H = H[1] ? H : ('0' + H[0]);
        var I = date.getMinutes().toString();
        I = I[1] ? I : ('0' + I[0]);
        var S = date.getSeconds().toString();
        S = S[1] ? S : ('0' + S[0]);
        return [D, M, Y].join('/') + ' ' + [H, I].join(':');
    },
    eval: function(val) {
        try {
            val = eval('(' + str.trim(val) + ')');
        } catch (e) {
            alert(val);
            //console.log(e);
            val = false;
        }
        return val;
    }
};

var formatBytes = function(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1000; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

var autoSizeText = function($object) {
    var el, elements, _i, _len, _results;
    elements = $object;
    if (elements.length < 0) {
        return;
    }
    _results = [];
    for (_i = 0, _len = elements.length; _i < _len; _i++) {
        el = elements[_i];
        _results.push((function(el) {
            var resizeText, _result;
            resizeText = function() {
                var elNewFontSize;
                elNewFontSize = (parseInt($(el).css('font-size').slice(0, -2)) - 1) + 'px';
                return $(el).css('font-size', elNewFontSize);
            };
            _result = [];
            while (el.scrollHeight > el.offsetHeight) {
                _result.push(resizeText());
            }
        })(el));
    }
};

var autoSizeTextarea = function(el, delay) {
    var observe;
    if (window.attachEvent) {
        observe = function(element, event, handler) {
            element.attachEvent('on'+event, handler);
        };
    }
    else {
        observe = function(element, event, handler) {
            element.addEventListener(event, handler, false);
        };
    }
    var resize = function() {
        el.style.height = 'auto';
        el.style.height = el.scrollHeight+'px';
        el.style.overflowY = 'hidden';
    }
    var resizeDelay = function() {
        var val = el.value;
        el.focus();
        el.value = '';
        el.value = val;
        window.setTimeout(resize, delay || 0);
    }
    //observe(el, 'drop',    resizeDelay);
    //observe(el, 'keydown', resizeDelay);
    //observe(el, 'cut',     resizeDelay);
    //observe(el, 'paste',   resizeDelay);
    observe(el, 'change',  resizeDelay);

    resize();
};

var mobileCheck = function() {
    var check = false;
    (function(a){
        var test = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4));
        if (test) {
            check = true;
        }
    })(navigator.userAgent || navigator.vendor|| window.opera);
    return check;
};

$(function() {
    if (typeof(toastr) != 'undefined') {
        toastr.options = {
          "closeButton": true,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-top-center",
          "preventDuplicates": false,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": 0,
          "extendedTimeOut": 0,
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut",
          "tapToDismiss": false
        };
    }
});

function initDataTable(table, url, notsearchable, notsortable, fnserverparams, defaultorder, pagination, rowreorder) {

    var _table_name = table;
    if ($(table).length == 0) {
        return;
    }
    if (fnserverparams == 'undefined' || typeof(fnserverparams) == 'undefined') {
        fnserverparams = []
    }
    var export_columns = [':visible'];
    // If not order is passed order by the first column
    if (typeof(defaultorder) == 'undefined') {
        defaultorder = [[0, 'ASC']];
    } else {
        if (typeof(defaultorder[0]) != 'object'){
            defaultorder = [defaultorder]
        }
    }
    for (i in defaultorder) {
        defaultorder[i][1] = typeof(defaultorder[i][1]) == 'string'? defaultorder[i][1].toLowerCase(): 'asc';
    }

    var buttons = [{
        extend: 'collection',
        text: 'Export',
        className: 'btn btn-default',
        buttons: [{
            extend: 'excelHtml5',
            text: 'Excel',
            exportOptions: {
                columns: export_columns,
            }

        }, {
            extend: 'csvHtml5',
            text: 'CSV',
            exportOptions: {
                columns: export_columns
            }

        }, {
            extend: 'pdfHtml5',
            text: 'PDF',
            orientation: 'landscape',
            customize: function(doc) {
                // Fix for column widths
                var table_api = table.DataTable();
                var columns = table_api.columns().visible();
                var columns_total = columns.length;
                var pdf_widths = [];
                for (i = 0; i < columns_total; i++) {
                    // Is only visible column
                    if (columns[i] == true) {
                        pdf_widths.push('*');
                    }
                }
                doc.styles.tableHeader.alignment = 'left'
                doc.styles.tableHeader.margin = [5, 5, 5, 5]
                doc.content[1].table.widths = pdf_widths;
                doc.pageMargins = [12, 12, 12, 12];

            },
            exportOptions: {
                columns: export_columns,
                stripNewlines: false
            }
        }, {
            extend: 'print',
            text: 'Print',
            autoPrint: false,
            exportOptions: {
                columns: export_columns,
                stripHtml: false
            },
            customize: function(win) {
                $(win.document.body).find('.noprint').hide();
                $(win.document.body).find('a').replaceWith(function() {
                    return $('<span />').append($(this).contents());
                });
                $(win.document.body).find('[style]').each(function() {
                    var style = $(this).attr('style');
                    if ($.trim(style) != '') {
                        style = style.replaceAll(';', ' !important;');
                    }
                    $(this).attr('style', style);
                });
                setTimeout(function() {
                    win.print();
                    win.close();
                }, 1000);
            }
        }],
    }, {
        extend: 'colvis',
        columns: ':not(.hide)',
        postfixButtons: ['colvisRestore'],
        className: 'btn btn-default dt-column-visibility',
        text: 'Visibility'
    }, {
        text: 'Reload',
        className: 'btn btn-default',
        action: function(e, dt, node, config) {
            dt.ajax.reload();
        }
    }];

    var length_options = [10, 25, 50, 100];
    var length_options_names = [10, 25, 50, 100];
    var tables_pagination_limit = 50;
    if ($.inArray(tables_pagination_limit, length_options) == -1) {
        length_options.push(tables_pagination_limit)
        length_options_names.push(tables_pagination_limit)
    }

    length_options.sort(function(a, b) {
        return a - b;
    });

    length_options_names.sort(function(a, b) {
        return a - b;
    });

    var notvisible = [];
    //Fal Edit not show hidden column by default
    $('body').find(table).find('th').each(function(i){
        if ($(this).hasClass('hidden')) {
            notvisible.push(i);
            $(this).removeClass('hidden');
        }
    });

    length_options.push(-1);
    length_options_names.push('All');
    var table = $('body').find(table).dataTable({
        "processing": true,
        "retrieve": true,
        "serverSide": typeof(url) == 'undefined' || url == 'undefined' || url === ''? false: true,
        'paginate': typeof(pagination) != 'undefined'? pagination: true,
        "rowReorder": typeof(rowreorder) == 'object' || rowreorder === true? rowreorder: false,
        'searchDelay': 400,
        "bDeferRender": true,
        "responsive": mobileCheck(),
        "autoWidth": false,
        dom: "<'mbot25'B><'row'><'row'<'col-md-6'l><'col-md-6'f>r>t<'row'<'col-md-4'i>><'row'<'#colvis'>p>",
        "pageLength": tables_pagination_limit,
        "lengthMenu": [length_options, length_options_names],
        buttons: buttons,
        "columnDefs": [{
            "searchable": false,
            "targets": notsearchable,
        }, {
            "sortable": false,
            "targets": notsortable
        }, {
            "visible": false,
            "targets": notvisible
        }],
        "drawCallback": function(oSettings) {
            if (typeof(datatables_draw) === 'function') {
                datatables_draw(table);
            }
        },
        "fnCreatedRow": function(nRow, aData, iDataIndex) {
            // If tooltips found
            if (typeof($('div').tooltip) !== 'undefined') {
                $(nRow).attr('data-title', aData.Data_Title);
                $(nRow).attr('data-toggle', aData.Data_Toggle);
                $(nRow).find('[data-toggle="tooltip"]').tooltip();
            }
        },
        "fnHeaderCallback": function(nHead, aData, iStart, iEnd, aiDisplay) {
            $(nHead).find('.mass_select_all_wrap').removeClass('hide');
        },
        "order": defaultorder,
        "ajax": typeof(url) == 'undefined' || url == 'undefined' || url === ''? null: {
            "url": url,
            "type": "POST",
            "data": function(d) {
                for (var key in fnserverparams) {
                    d[key] = $(fnserverparams[key]).val();
                }
            }
        }
    });

    // mass-delete button have addition btn-default class which is added from datatables we need to remove in case user make custom style for buttons
    $('.dataTables_wrapper .dt-buttons').find('._mass_delete').removeClass('btn-default');
    $('body').find('._mass_delete').attr('data-placement', 'bottom');
    $('body').find('._mass_delete').attr('data-toggle', 'tooltip');
    $('body').find('._mass_delete').attr('title', 'Use the checkboxes on the right side for mass delete.');

    $('.dataTables_info').addClass('pull-left hidden-xs').css('margin-left', '15px');
    $('.dataTables_info').prependTo($('.dataTables_paginate').parent());

    $('.dataTables_wrapper .dt-buttons').addClass('pull-right').css('margin', '2px 15px');;
    $('.dataTables_wrapper .dt-buttons').prependTo($('.dataTables_paginate').parent());

    $('.dataTables_filter [type=search]').attr('placeholder', 'Search').prependTo('.dataTables_filter');
    $('.dataTables_filter label').remove();

    $('.dataTables_filter').parent().removeClass('col-md-6').addClass('col-md-9');
    $('.dataTables_length').parent().removeClass('col-md-6').addClass('col-md-3');

    $('<div>').addClass('visible-xs').css({'clear':'both', 'margin-bottom':'5px'}).insertAfter($('#colvis'));
    $('<style>').html('@media (max-width: 767px) { .dataTables_wrapper .dt-buttons { margin: 0 !important } }').insertAfter($('#colvis'));

    // hide reload button for non ajax data
    if (typeof(url) == 'undefined' || url == 'undefined' || url === '') {
        $('.dataTables_wrapper .dt-buttons a:Contains("Reload")').remove();
    }

    setTimeout(function() {
        // Fix for hidden tables colspan not correct if the table is empty
        if ($(_table_name).is(':hidden')) {
            $(_table_name).find('.dataTables_empty').attr('colspan', $(_table_name).find('thead th').length);
        }
    }, 1000);

    return table.DataTable();
};
