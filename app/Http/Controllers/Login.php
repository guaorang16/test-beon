<?php

namespace App\Http\Controllers;

use DB;
use Response;
use Request;
use App\Whmcs;

class Login extends Controller
{

    public $redirect;

    public function __construct() {
        $this->redirect = url('/.');
        
        if (request()->has('url')) {
            $this->redirect = request()->get('url');
        }
    }

    public function authenticate() {
        $user = request()->input('email');
        $pass = request()->input('password');
        $error = null;

        if ($user) {
            //$res = \App\Whmcs::getData('ValidateLogin', request()->all());
            $res = \App\Whmcs::getData('GetClientsDetails', request()->all());

            if (isset($res['userid'])) {
                //return response()->json($res);

                request()->session()->put('userid', $res['userid']);
                request()->session()->put('usermail', $res['email']);
                request()->session()->put('username', $res['fullname']);

                return redirect($this->redirect);
            }

            $error = 'Invalid username or password';
        }

        return view('admin/login', [
            'error' => $error
        ]);
    }

    public function relogin() {
        request()->session()->forget('userid');

        return redirect('/login');
    }

    public function register()
    {
        $error = null;

        $param = [
            'firstname' => 'John',
            'lastname' => 'Doe',
            'email' => 'john.doe@example.com',
            'address1' => '123 Main Street',
            'city' => 'Anytown',
            'state' => 'State',
            'postcode' => '12345',
            'country' => 'US',
            'phonenumber' => '800-555-1234',
            'password2' => 'password'
        ];

        $res = Whmcs::getData('AddClient', $param);

        if (request()->input()) {
            if (isset($res['clientid'])) {
                return redirect('/login')->with('status', 'Akun berhasil dibuat. Silahkan Login');
            }
            $error = $res['result'] ?? 'Terjadi Error API';
            return back()->with('error', $error);
        }

        return view('admin/regis', [
            'status' => $error
        ]);
        // return back()->with('error','Akun Gagal dibuat. Silahkan Cek Kembali');
    }
}