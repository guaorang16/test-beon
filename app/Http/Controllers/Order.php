<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Whmcs;

class Order extends Controller
{
     public function __construct() {
        $this->middleware([\App\Http\Middleware\ValidateLogin::class, 'login']);
    }

	public static function test() {
		return response()->json(Whmcs::getData('GetProducts'), 200);
	}
    
    public function submit() {
        $input = request()->get('send');

        return view('order_index', ['data' => $input]);
    }

    public function list() {
        $order = Whmcs::getData('GetOrders', ['userid' => request()->session()->get('userid')]);
        if (isset($order['orders'])) {
            $order = $order['orders']['order'];
        } else {
            $order = [];
        }

        return view('order_index', ['order' => $order]);
    }

    public function details($id = '') {
        return response()->json(Whmcs::getData('GetOrders',
            ['id' => $id]
        ));
    }

    public function detail($id = '') {

        $res = Whmcs::getData('GetOrders', ['id' => $id]);
        if (isset($res['orders'])) {
            $order          = $res['orders']['order'][0] ?? [];
            $ordernum       = $order? $order['ordernum'] : '';
            $name           = $order? $order['name'] : '';
            $paymentmethod  = $order? $order['paymentmethod'] : '';
            $status         = $order? $order['status'] : '';
            $currencyprefix = $order? $order['currencyprefix'] : '';
            $amount         = $order? $order['amount'] : '';
        } else {
            $order = [];
        }
        if (isset($res['orders']['order'][0]['lineitems']['lineitem'])) {
            $lineitem = $res['orders']['order'][0]['lineitems']['lineitem'];
        } else {
            $lineitem = [];
        }

        return view('admin/order_detail', [
            'ordernum' => $ordernum,
            'name' => $name,
            'paymentmethod' => $paymentmethod,
            'status' => $status,
            'currencyprefix' => $currencyprefix,
            'amount' => $amount,
            'lineitem' => $lineitem
        ]);
    }

    public function save() {
        $param = [
            'clientid' => request()->session()->get('userid'),
            'paymentmethod' => 'gopay',
            'domain' => 'coba.co.id',
            'billingcycle' => 'annualy',
            'pid' => 527
        ];

        $res = Whmcs::getData('AddOrder', $param);

        if (isset($res['orderid'])) {
            return redirect('/detail_order/' . $res['orderid']);
        }

        return response()->json($res);
    }
}
