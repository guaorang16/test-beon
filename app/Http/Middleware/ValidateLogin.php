<?php

namespace App\Http\Middleware;

use Closure;
 
class ValidateLogin
{
    
    public function handle($request, Closure $next) {
        // Some action before request
        if (!$request->session()->get('userid')) {
            return redirect('/login?url=' . request()->url());
        }

        // Do the request
        $response = $next($request);
 
        // Some action after request
        $dosomething = true;

        // Return the request
        return $response;
    }
 
}
